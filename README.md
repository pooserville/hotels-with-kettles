# Hotels with Kettles

_Which hotels in the world provide kettles in the room?_

Alphabetical by country, then city, then hotel name.

## Australia

* Hobart, TAS
    * [Hadley's Orient Hotel](http://hadleyshotel.com.au)
* Melbourne, VIC
    * [DoubleTree by Hilton Hotel Melbourne - Flinders Street](https://doubletree3.hilton.com/en/hotels/victoria/doubletree-by-hilton-hotel-melbourne-flinders-street-MELFSDI/index.html)

## Netherlands
 
 * Amsterdam
 	* [Apollofirst Boutique Hotel](https://www.apollofirst.nl/en/index.html)
 	* [Olympic Hotel](https://www.olympichotel.nl/en/home)